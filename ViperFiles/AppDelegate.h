//
//  AppDelegate.h
//  ViperFiles
//
//  Created by Elvira Khab on 02.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

