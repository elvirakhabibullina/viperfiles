//
//  BillsAssembly.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 26.02.18.
//  Copyright © 2018 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TemplateModuleViewInput.h"
#import "TemplateModuleRouterInput.h"
#import "TemplateModuleModuleOutput.h"

@interface TemplateModuleAssembly : NSObject

@property (nonatomic, strong, nonnull) id<TemplateModuleViewInput> view;
@property (nonatomic, weak, nullable) id<TemplateModuleRouterInput> router;

//+ (nonnull BillsAssembly *)assembleWithModuleOutput:(nonnull id <BillsModuleOutput>)moduleOutput;
+ (nonnull TemplateModuleAssembly *)assemble:(NSString *_Nonnull)appartmentId;

@end
