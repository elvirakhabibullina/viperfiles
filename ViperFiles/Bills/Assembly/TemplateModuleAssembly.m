//
//  TemplateModuleAssembly.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 26.02.18.
//  Copyright © 2018 Дмитрий Гутовец. All rights reserved.
//

#import "TemplateModuleAssembly.h"

#import "TemplateModuleRoute.h"
#import "TemplateModuleInteractor.h"
#import "TemplateModulePresenter.h"
#import "TemplateModuleView.h"

@implementation TemplateModuleAssembly

+ (nonnull TemplateModuleAssembly *)assemble:(NSString *_Nonnull)appartmentId {
    id<TemplateModuleRouterInput> router = [[TemplateModuleRoute alloc] init];
    id<TemplateModuleInteractorInput> interactor = [[TemplateModuleInteractor alloc] init];
//    NetworkManager *network = [[NetworkManager alloc] init];
//    id<TemplateModuleServiceInterface> service = [[TemplateModuleService alloc] initWithNetwork:network];
    id<TemplateModuleInteractorOutput, TemplateModuleViewOutput> presenter = [[TemplateModulePresenter alloc] init];
    id<TemplateModuleViewInput> view = [[TemplateModuleView alloc] initWithNibName:@"TemplateModuleView" bundle:nil];
//    TemplateModuleDataDecorator *builder = [[BillDataDecorator alloc] init];
    
    ((TemplateModuleView *)view).output = presenter;
    ((TemplateModuleInteractor *)interactor).output = presenter;
//    ((TemplateModuleInteractor *)interactor).templateModuleService = service;
//    ((TemplateModuleInteractor *)interactor).builder = builder;
    ((TemplateModulePresenter *)presenter).router = router;
    ((TemplateModulePresenter *)presenter).interactor = interactor;
    ((TemplateModulePresenter *)presenter).view = view;
//    router.navigationHandler = (UIViewController *)view;
    
    TemplateModuleAssembly *module = [[TemplateModuleAssembly alloc] init];
    module.view = view;
    module.router = router;
    return module;
}

@end
