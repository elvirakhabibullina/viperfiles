//
//  TemplateModuleInteractor.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 26.02.18.
//  Copyright © 2018 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TemplateModuleInteractorInput.h"
#import "TemplateModuleInteractorOutput.h"

@interface TemplateModuleInteractor : NSObject <TemplateModuleInteractorInput>

@property (nonatomic, weak) id<TemplateModuleInteractorOutput> output;

@end
