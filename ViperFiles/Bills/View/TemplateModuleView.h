//
//  CheckView.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 26.02.18.
//  Copyright © 2018 Дмитрий Гутовец. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateModuleViewInput.h"
#import "TemplateModuleViewOutput.h"

@interface TemplateModuleView : UIViewController <TemplateModuleViewInput>

@property (nonatomic, strong) id<TemplateModuleViewOutput> output;

@end
