//
//  TemplateModulePresenter.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 26.02.18.
//  Copyright © 2018 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TemplateModuleInteractorInput.h"
#import "TemplateModuleInteractorOutput.h"
#import "TemplateModuleViewInput.h"
#import "TemplateModuleViewOutput.h"
#import "TemplateModuleRouterInput.h"

@interface TemplateModulePresenter : NSObject <TemplateModuleInteractorOutput, TemplateModuleViewOutput>

@property (nonatomic, weak) id<TemplateModuleViewInput> view;
@property (nonatomic, strong) id<TemplateModuleRouterInput> router;
@property (nonatomic, strong) id<TemplateModuleInteractorInput> interactor;

@end
