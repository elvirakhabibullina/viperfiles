//
//  LogInViewProtocol.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol LogInViewInput <NSObject>

- (void)drawFieldLogin:(NSDictionary *)dict;
- (void)drawFieldPass:(NSDictionary *)dict;
- (void)hideKeyboardInView;
- (void)updateBtnSendState:(NSInteger)state;
- (void)loginWithError:(NSDictionary *)error;
- (void)alertRecoveryFinish:(NSDictionary *)data;
- (void)updateFieldLoginValue:(NSString *)value;
- (void)updateFieldPassValue:(NSString *)value;

- (void)showLoading;
- (void)dismissLoading;

-(void)updateContent;

@end
