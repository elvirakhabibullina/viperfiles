//
//  LogInView.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogInViewInput.h"
#import "LogInPresenterProtocol.h"


@interface LogInView : UIViewController <LogInViewInterface>

@property (weak, nonatomic) id<LogInPresenterProtocol> presenter;

@end
