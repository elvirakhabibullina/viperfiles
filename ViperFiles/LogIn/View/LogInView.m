//
//  LogInView.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import "LogInView.h"

//#import "UINavigationController+Style.h"
//#import "BaseTextField.h"
//#import "BaseButton.h"
#import "TitleView.h"
#import "AlertController.h"

@interface LogInView () //<BaseTextFieldDelegate>

//@property (strong, nonatomic) BaseTextField *fieldLogin;
//@property (strong, nonatomic) BaseTextField *fieldPass;
//@property (strong, nonatomic) BaseButton *btnSend;
@property (strong, nonatomic) UIButton *btnRecovery;

@property (nonatomic, strong) UIView *loadingView;

@end

@implementation LogInView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    [self.navigationController styledByStyleGuide];
    
    [self configure];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.title = @"";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title = @"";
}

- (void)configure
{
    [self.presenter displayNeedControls];
    
//    self.btnSend = [BaseButton buttonWithState:BaseButtonStateDissabled];
//    self.btnSend.frame = [self.presenter frameButtonSend:[self.btnSend defaultFrame]];
//    [self.btnSend setTitle:[self.presenter titleBtnSendText] forState:UIControlStateNormal];
//    [self.btnSend addTarget:self action:@selector(tapSend:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:self.btnSend];
    
    NSDictionary *btnSendDict = [self.presenter dataOfButtonRecovery];
    self.btnRecovery = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.btnRecovery.frame = [self.presenter frameButtonRecovery];
    [self.btnRecovery setTitle:btnSendDict[@"title"] forState:UIControlStateNormal];
    [self.btnRecovery setTitleColor:btnSendDict[@"color"] forState:UIControlStateNormal];
    [self.btnRecovery addTarget:self action:@selector(tapRecovery:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnRecovery];
}

- (void)testUpdate {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NEED_UPDATE_WINDOW_NOTIFICATION" object:nil];
}


#pragma mark Actions
- (void)tapSend:(UIButton *)sender
{
    [self.presenter logIn];
}

- (void)tapRecovery:(UIButton *)sender
{
    [self.presenter moveToRecovery];
    // [self.presenter moveToDashboard];
}


#pragma mark BaseTextFieldDelegate
- (void)textField:(NSString *)textFieldIdentifier changeValue:(NSString *)valueString
{
    [self.presenter validateTextField:textFieldIdentifier withValue:valueString];
}


#pragma mark LogInViewInterface
- (void)drawFieldLogin:(NSDictionary *)dict
{
//    self.fieldLogin = [BaseTextField fieldWithData:dict];
//    [self.view addSubview:self.fieldLogin];
//    self.fieldLogin.frame = [self.presenter frameForLoginField];
//    self.fieldLogin.delegate = self;
}

- (void)drawFieldPass:(NSDictionary *)dict
{
//    self.fieldPass = [BaseTextField fieldWithData:dict];
//    [self.view addSubview:self.fieldPass];
//    self.fieldPass.frame = [self.presenter frameForPassField];
//    self.fieldPass.delegate = self;
}

- (void)hideKeyboardInView
{
//    [self.fieldLogin hideResponder];
//    [self.fieldPass hideResponder];
}

- (void)updateBtnSendState:(NSInteger)state
{
//    self.btnSend.stateButton = state;
}

- (void)updateFieldLoginValue:(NSString *)value
{
//    [self.fieldLogin changeValue:value];
}

- (void)updateFieldPassValue:(NSString *)value
{
//    [self.fieldPass changeValue:value];
}

- (void)loginWithError:(NSDictionary *)error
{
//    AlertController *alertView = [AlertController alertControllerWithDict:error];
//    [alertView showInViewController:self];
}

- (void)alertRecoveryFinish:(NSDictionary *)data
{
    [self loginWithError:data];
}

-(void)showLoading {
    if (![self.navigationController.view.subviews containsObject:self.loadingView]) {
        self.loadingView = [[[NSBundle mainBundle] loadNibNamed:@"LaunchXIB" owner:self options:nil] objectAtIndex:0];
        [self.loadingView setFrame:self.navigationController.view.bounds];
        [self.navigationController.view addSubview:self.loadingView];
    }
}

-(void)dismissLoading {
    [self.loadingView removeFromSuperview];
}

-(void)updateContent {
    [self.presenter displayNeedControls];
//    [self.btnSend setTitle:[self.presenter titleBtnSendText] forState:UIControlStateNormal];
    NSDictionary *btnSendDict = [self.presenter dataOfButtonRecovery];
    [self.btnRecovery setTitle:btnSendDict[@"title"] forState:UIControlStateNormal];
}

#pragma mark LogInViewInterface





@end

