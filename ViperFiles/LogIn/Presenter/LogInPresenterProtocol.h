//
//  LogInPresenterProtocol.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

@protocol LogInPresenterProtoco <NSObject>

// Common config

// Interface
- (void)displayNeedControls;

// Controls
- (CGRect)frameForLoginField;
- (CGRect)frameForPassField;
- (CGRect)frameButtonSend:(CGRect)defaultFrame;
- (NSString *)titleBtnSendText;
- (CGRect)frameButtonRecovery;
- (NSDictionary *)dataOfButtonRecovery;
- (void)updateFieldLoginValue:(NSString *)value;

// Actions
- (void)validateTextField:(NSString *)identifier withValue:(NSString *)value;
- (void)moveToRecovery;
- (void)moveToDashboard;
- (void)logIn;
- (void)finishRecoveryEmail:(NSDictionary *)data;

// Getters

-(NSDictionary *)configurationForRecovery;

@end
