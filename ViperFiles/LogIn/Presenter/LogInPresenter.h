//
//  LogInPresenter.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LogInViewInput.h"
#import "LogInViewOutput.h"
#import "LogInInteractorInput.h"
#import "LogInInteractorOutput.h"
#import "LogInRouterInput.h"
#import "LogInModuleOutput.h"


@interface LogInPresenter : NSObject <LogInViewOutput, LogInInteractorOutput>

@property (strong, nonatomic) id<LogInInteractorInput> interactor;
@property (nonatomic, weak) id<LogInViewInput> view;
@property (nonatomic, strong) id<LogInRouterInput> router;
@property (nonatomic, strong) id<LogInInteractorInput> interactor;
@property (nonatomic, strong) id<LogInModuleOutput> moduleOutput;


@end
