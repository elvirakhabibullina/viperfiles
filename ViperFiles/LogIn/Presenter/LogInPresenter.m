//
//  LogInPresenter.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import "LogInPresenter.h"

@interface LogInPresenter()

@end

@implementation LogInPresenter

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        
    }
    
    return self;
}

#pragma mark LogInViewOutput


#pragma mark LogInPresenterProtocol - Interface
- (void)displayNeedControls
{
    NSDictionary *dictLogin = [self.interactor dataOfLoginField];
    [self.view drawFieldLogin:dictLogin];
    
    NSDictionary *dictPass = [self.interactor dataOfPassField];
    [self.view drawFieldPass:dictPass];
}


#pragma mark LogInPresenterProtocol - Controls
- (CGRect)frameForLoginField
{
    return self.rectLoginField;
}

- (CGRect)frameForPassField
{
    return self.rectPassField;
}

- (CGRect)frameButtonSend:(CGRect)defaultFrame
{
    _rectBtnSend = CGRectMake(defaultFrame.origin.x, (_rectPassField.origin.y + _rectPassField.size.height + 30.), defaultFrame.size.width, defaultFrame.size.height);
    return self.rectBtnSend;
}

- (NSString *)titleBtnSendText
{
    return [LocalizedStrings stringForKey:@"login"];
}

- (CGRect)frameButtonRecovery
{
    CGRect frame = self.rectBtnSend;
    frame.origin.y += 20.f + frame.size.height;
    frame.size.height = 33.f;
    
    return frame;
}

- (NSDictionary *)dataOfButtonRecovery
{
    return @{@"title": [LocalizedStrings stringForKey:@"password.forgot"], @"color": [UIColor buttonRecoveryColor]};
}

-(NSDictionary *)configurationForRecovery {
    return [self.interactor configurationForRecovery];
}

#pragma mark LogInPresenterProtocol - Actions
- (void)validateTextField:(NSString *)identifier withValue:(NSString *)value
{
    [self.interactor validateTextField:identifier withValue:value];
}

- (void)moveToRecovery
{
    [self.view hideKeyboardInView];
    [self.wireframe pushRecovery];
}

- (void)moveToDashboard
{
    [self.view hideKeyboardInView];
    [self.wireframe pushDashboard:YES completion:nil];
}

- (void)logIn
{
    [self.interactor sentLogin];
}

- (void)finishRecoveryEmail:(NSDictionary *)data
{
    [self.interactor preSetFieldLoginValue:data];
    
    if(![data[@"title"] fieldIsGood] && ![data[@"message"] fieldIsGood]) return;
    [self.view alertRecoveryFinish:data];
}


#pragma mark LogInInteractorOutput
- (void)validatiotFieldResult:(NSString *)identifier withStates:(BaseButtonState)states
{
    [self.view updateBtnSendState:states];
}

- (void)loginStatus:(BOOL)status withResult:(NSDictionary *)result
{
    if(status)
    {
        [self moveToDashboard];
    }
    else
    {
        [self.view loginWithError:result];
    }
}

- (void)updateFieldLoginValue:(NSString *)value
{
    [self.view updateFieldLoginValue:value];
}

- (void)blockedButtomSend:(BaseButtonState)states
{
    [self.view updateBtnSendState:states];
}

- (void)cleanFields
{
    [self.view updateFieldLoginValue:@""];
    [self.view updateFieldPassValue:@""];
    
}

-(void)updateContent {
    [self.view updateContent];
}

@end
