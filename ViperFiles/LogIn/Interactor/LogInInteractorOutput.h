//
//  LogInInteractorOutput.h
//  ViperFiles
//
//  Created by Elvira Khab on 20.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

@protocol LogInInteractorOutput <NSObject>

- (void)validatiotFieldResult:(NSString *)identifier withStates:(BaseButtonState)states;
- (void)loginStatus:(BOOL)status withResult:(NSDictionary *)result;
- (void)updateFieldLoginValue:(NSString *)value;
- (void)blockedButtomSend:(BaseButtonState)states;
- (void)cleanFields;

-(void)updateContent;

@end
