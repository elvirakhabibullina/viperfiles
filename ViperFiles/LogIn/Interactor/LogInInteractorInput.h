//
//  LogInInteractorProtocol.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "BaseButtonHeader.h"

@protocol LogInInteractorInput <NSObject>

- (NSDictionary *)dataOfLoginField;
- (NSDictionary *)dataOfPassField;
- (void)validateTextField:(NSString *)identifier withValue:(NSString *)value;
- (void)sentLogin;
- (void)preSetFieldLoginValue:(NSDictionary *)valueDict;
- (NSDictionary *)configurationForRecovery;
@end



