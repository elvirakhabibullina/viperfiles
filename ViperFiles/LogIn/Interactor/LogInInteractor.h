//
//  LogInInteractor.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LogInInteractorInput.h"

@interface LogInInteractor : NSObject <LogInInteractorInput>

@property (weak, nonatomic) id<LogInInteractorOutput> output;

@end
