//
//  LogInInteractor.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import "LogInInteractor.h"

#import "BaseTextFieldEntity.h"
#import "WorkerValidation.h"
#import "AuthManager.h"
#import <UIKit/UIKit.h>
#import "UserInfo.h"
#import "NSString+PhoneValidation.h"
#import "NSString+PhoneFormatter.h"

#import "DeviceManager.h"
#import "LanguagesManager.h"

#define idLogin @"login"
#define idPass @"password"

#import "PayConfigManager.h"

@interface LogInInteractor()

@property (strong, nonatomic) BaseTextFieldEntity *entityLogin;
@property (strong, nonatomic) BaseTextFieldEntity *entityPass;
@property (nonatomic) BOOL isValidLogin;
@property (nonatomic) BOOL isValidPass;

//@property (strong, nonatomic) WorkerValidation *worker;
@property (strong, nonatomic) AuthManager *manager;

@end

@implementation LogInInteractor

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _isValidLogin = NO;
        _isValidPass = NO;
        _worker = [[WorkerValidation alloc] init];
        
        [self configure];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(languageDidUpdate) name:LANGUAGE_DID_CHANGE_NOTIFICATION_NAME object:nil];
    }
    
    return self;
}

- (void)configure
{
    NSDictionary *dictLogin = @{@"identifier": idLogin, @"title": [LocalizedStrings stringForKey:@"emailOrPhone"], @"position": @[@1, @0], @"capitalizationType": @(UITextAutocapitalizationTypeNone), @"keyboard": @(UIKeyboardTypeEmailAddress)};
    self.entityLogin = [[BaseTextFieldEntity alloc] initWidthDict:dictLogin];
    
    NSDictionary *dictPass = @{@"identifier": idPass, @"title": [LocalizedStrings stringForKey:@"password"], @"position": @[@0, @1], @"type": @(BaseTextFieldTypePass)};
    self.entityPass = [[BaseTextFieldEntity alloc] initWidthDict:dictPass];
}

-(NSDictionary *)configurationForRecovery {
    return [self.worker validLoginField:self.entityLogin.value] ? @{@"login": self.entityLogin.value} : nil;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)isValidLogin
{
    return [self.worker validLoginField:self.entityLogin.value];
}

- (BOOL)isValidPass
{
    return [self.worker validPassField:self.entityPass.value];
}

- (BOOL)saveToken:(NSDictionary *)data
{
    if([data isDict])
    {
        if([data[@"token"] fieldIsGood])
        {
            NSString *token = [data[USER_TOKEN_KEY] string];
            [[UserInfo sharedInstanse] setObject:token forKey:USER_TOKEN_KEY];
            [[UserInfo sharedInstanse] saveData];
            
            [self.output cleanFields];
            
            return YES;
        }
    }
    
    return NO;
}

- (BaseTextFieldEntity *)entityForIdentifier:(NSString *)identifier
{
    if([identifier isEqualToString:idLogin])
    {
        return self.entityLogin;
    }
    else if([identifier isEqualToString:idPass])
    {
        return self.entityPass;
    }
    
    return nil;
}

- (void)updateValueField:(NSString *)identifier andValue:(NSString *)value
{
    BaseTextFieldEntity *tmpEntity = [self entityForIdentifier:identifier];
    if(tmpEntity != nil)
    {
        tmpEntity.value = ([value isString]) ? value : @"";
    }
}

#pragma mark LogInInteractorInput
- (NSDictionary *)dataOfLoginField
{
    return [self.entityLogin convertToDictionary];
}

- (NSDictionary *)dataOfPassField
{
    return [self.entityPass convertToDictionary];
}

- (void)validateTextField:(NSString *)identifier withValue:(NSString *)value
{
    // Validation
    [self updateValueField:identifier andValue:value];
    
    BaseButtonState validationState = BaseButtonStateDissabled;
    if(self.isValidLogin && self.isValidPass)
    {
        validationState = BaseButtonStateEnabled;
    }
    [self.output validatiotFieldResult:identifier withStates:validationState];
}

- (void)preSetFieldLoginValue:(NSDictionary *)valueDict
{
    if([valueDict[@"value"] isString])
    {
        [self.output updateFieldLoginValue:valueDict[@"value"]];
    }
}

- (void)sentLogin
{
    [self.output blockedButtomSend:BaseButtonStateDissabled];
    
    if(self.manager == nil)
    {
        self.manager = [[AuthManager alloc] init];
    }
    
    // На бэке изменился формат телефона
    NSString *fixedValue = self.entityLogin.value;
//    if ([self.entityLogin.value isValidPhone]) {
//        NSString *rawPhone = [self.entityLogin.value rawPhoneNumber];
//        fixedValue = [NSString stringWithFormat:@"7%@", rawPhone];
//    }
    [self.manager loginUserWithName:fixedValue andPassword:self.entityPass.value completion:^(NSString *error, NSDictionary *response) {
        NSDictionary *errorDict;
        BOOL status;
        
        if(error == nil) {
            errorDict = nil;
            status = YES;
            
            if(![self saveToken:response]) {
                status = NO;
                errorDict = @{@"title": [LocalizedStrings stringForKey:@"error.title"], @"message": [LocalizedStrings stringForKey:@"Произошла неизвестная ошибка"]};
            }
            
            [[PayConfigManager manager] refreshProviders];
        } else {
            if([error isEqualToString:@"notreachable"])
            {
                errorDict = @{@"title": [LocalizedStrings stringForKey:@"error.noInternet"]};
            }
            else
            {
                errorDict = @{@"title": [LocalizedStrings stringForKey:@"error.title"], @"message": error};
            }
            status = NO;
        }
        if (status) {
            [[DeviceManager manager] linkUserToDevice];
            [self.output blockedButtomSend:BaseButtonStateDissabled];
        } else {
            [self.output blockedButtomSend:BaseButtonStateEnabled];
        }
        [self.output loginStatus:status withResult:errorDict];
        
    }];
}

-(void)languageDidUpdate {
    [self configure];
    [self.output updateContent];
}


@end
