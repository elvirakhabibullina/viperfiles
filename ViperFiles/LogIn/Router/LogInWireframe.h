//
//  LogInWireframe.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class CommonWireframe;
@class LogInPresenter;

@interface LogInWireframe : NSObject

@property (strong, nonatomic) LogInPresenter *presenter;
@property (strong, nonatomic) CommonWireframe *rootWireframe;

- (void)prepareLogin:(UIWindow *)window;
- (void)processLogin;
- (void)pushRecovery;
- (void)pushDashboard:(BOOL)animate completion:(void(^)(void))completion;
- (void)presentSplash:(BOOL)animate completion:(void(^)(void))completion;
- (void)popToRootWireframeFrome:(UIViewController *)viewController;


@end
