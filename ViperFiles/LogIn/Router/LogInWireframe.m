//
//  LogInWireframe.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 06.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import "LogInWireframe.h"

#import "LogInView.h"
#import "LogInPresenter.h"
#import "CommonWireframe.h"
#import "RecoveryWireframe.h"
#import "RecoveryPresenter.h"
#import "RecoveryInteractor.h"

#import "DashboardWireframe.h"
#import "DashboardPresenter.h"
#import "DashboardInteractor.h"

#import "SplashWireframe.h"
#import "SplashPresenter.h"
#import "SplashInteractor.h"


#import "UserInfo.h"

@interface LogInWireframe () <RecoveryWireframeDelegate>

@property (strong, nonatomic) RecoveryWireframe *recoveryWireframe;
@property (strong, nonatomic) DashboardWireframe *dashboardWireframe;
@property (strong, nonatomic) SplashWireframe *splashWireframe;
@property (weak, nonatomic) LogInView *loginView;

@end

@implementation LogInWireframe



- (LogInView *)loginViewController
{
    LogInView *view = [[LogInView alloc] initWithNibName:@"LogInView" bundle:nil];
    return view;
}


#pragma mark Private

- (void)pushRecoveryConfig
{
    RecoveryWireframe *wireframe = [[RecoveryWireframe alloc] init];
    RecoveryPresenter *presenter = [[RecoveryPresenter alloc] init];
    RecoveryInteractor *interactor = [[RecoveryInteractor alloc] initWithRootConfig:[self.presenter configurationForRecovery]];
    
    interactor.output = presenter;
    presenter.interactor = interactor;
    
    wireframe.presenter = presenter;
    presenter.wireframe = wireframe;
    
    wireframe.rootWireframe = self.rootWireframe;
    self.recoveryWireframe = wireframe;
    self.recoveryWireframe.delegate = self;
}

- (void)pushDashboardConfig
{
    DashboardWireframe *wireframe = [[DashboardWireframe alloc] init];
    DashboardPresenter *presenter = [[DashboardPresenter alloc] init];
    DashboardInteractor *interactor = [[DashboardInteractor alloc] init];
    
    interactor.output = presenter;
    presenter.interactor = interactor;
    
    wireframe.presenter = presenter;
    presenter.wireframe = wireframe;
    
    wireframe.rootWireframe = self.rootWireframe;
    self.dashboardWireframe = wireframe;
}

- (void)splashConfig
{
    if(self.splashWireframe != nil) return;
    
    SplashWireframe *wireframe = [[SplashWireframe alloc] init];
    SplashPresenter *presenter = [[SplashPresenter alloc] init];
    SplashInteractor *interactor = [[SplashInteractor alloc] init];
    
    interactor.output = presenter;
    presenter.interactor = interactor;
    
    wireframe.presenter = presenter;
    presenter.wireframe = wireframe;
    
    wireframe.rootWireframe = self.rootWireframe;
    self.splashWireframe = wireframe;
}


#pragma mark RecoveryWireframeDelegate
- (void)recoveryResponse:(NSDictionary *)response
{
    [self.presenter finishRecoveryEmail:response];
}


#pragma mark Public
- (void)prepareLogin:(UIWindow *)window
{
    LogInView *vc = [self loginViewController];
    
    self.presenter.view = vc;
    vc.presenter = self.presenter;
    
    [self.rootWireframe rootViewController:vc forWindow:window andWireframe:self];
    self.loginView = vc;
    [self.loginView showLoading];
}

-(void)processLogin {
    NSString *token = [[UserInfo sharedInstanse] objectForKey:USER_TOKEN_KEY];
    if([token isString])
    {
        NSLog(@"PUSH");
        [self pushDashboard:NO completion:^{
            NSLog(@"Completion!!");
            [self.loginView dismissLoading];
        }];
    }
    else
    {
        [self presentSplash:YES completion:^{
            [self.loginView dismissLoading];
        }];
    }
}

- (void)pushRecovery
{
    [self pushRecoveryConfig];
    
    [self.recoveryWireframe pushRecoveryFromLogIn:self.rootWireframe.navigationController];
}

- (void)pushDashboard:(BOOL)animate completion:(void(^)(void))completion
{
    [self pushDashboardConfig];
    
    [self.dashboardWireframe pushDashboardFromLogIn:self.rootWireframe.navigationController withAnimation:animate completion:completion];
}

- (void)presentSplash:(BOOL)animate completion:(void(^)(void))completion
{
    [self splashConfig];
    
    [self.splashWireframe presentFromView:self.rootWireframe.navigationController withAnimation:animate completion:completion];
}

- (void)popToRootWireframeFrome:(UIViewController *)viewController
{
    [self splashConfig];
    [self.splashWireframe presentSelfFromView:self.loginView withAnimation:YES completion:nil];
}


@end
