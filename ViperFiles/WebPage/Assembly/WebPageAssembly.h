//
//  WebPageAssembly.h
//  ViperFiles
//
//  Created by Elvira Khab on 19.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebPageViewInput.h"
#import "WebPageRouteInput.h"
#import "WebPageModuleOutput.h"

@interface WebPageAssembly : NSObject

@property (nonatomic, strong, nonnull) id<WebPageViewInput> view;
@property (nonatomic, weak, nullable) id<WebPageRouteInput> router;

+ (WebPageAssembly *)assembleWithModuleOutput:(id<WebPageModuleOutput>)moduleOutput;

@end
