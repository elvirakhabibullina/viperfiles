//
//  WebPageAssembly.m
//  ViperFiles
//
//  Created by Elvira Khab on 19.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import "WebPageAssembly.h"
#import "WebPageRouteInput.h"
#import "WebPageRoute.h"
#import "WebPageInteractorInput.h"
#import "WebPageInteractorOutput.h"
#import "WebPageInteractor.h"
#import "WebPageViewInput.h"
#import "WebPageViewOutput.h"
#import "WebPageView.h"
#import "WebPagePresenter.h"

@implementation WebPageAssembly

+ (WebPageAssembly *)assembleWithModuleOutput:(id<WebPageModuleOutput>)moduleOutput {
    
    id<WebPageRouteInput> router = [[WebPageRoute alloc] init];
    id<WebPageInteractorInput> interactor = [[WebPageInteractor alloc] init];
    id<WebPageInteractorOutput, WebPageViewOutput, WebPageRouteOutput> presenter = [[WebPagePresenter alloc] init];
    
    
    id<WebPageViewInput> view = [[WebPageView alloc] initWithNibName:@"WebPageView" bundle:nil];
//    WebPageManager *manager = [WebPageManager new];
    
    ((WebPageView *)view).output = presenter;
    ((WebPageInteractor *)interactor).output = presenter;
    ((WebPagePresenter *)presenter).router = router;
    ((WebPagePresenter *)presenter).interactor = interactor;
    ((WebPagePresenter *)presenter).view = view;
    ((WebPagePresenter *)presenter).moduleOutput = moduleOutput;
    
    ((WebPageRoute *)router).output = presenter;
    
    WebPageAssembly *module = [[WebPageAssembly alloc] init];
    module.view = view;
    module.router = router;
    return module;
}

@end
