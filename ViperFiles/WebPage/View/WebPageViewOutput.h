//
//  WebPageViewOutput.h
//  ViperFiles
//
//  Created by Elvira Khab on 19.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

@protocol WebPageViewOutput <NSObject>

-(void)requestPageInfo;
-(void)close;

@end
