//
//  WebPageProtocol.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 09.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//


@protocol WebPageViewInput <NSObject>

-(void)openURL:(NSURL *)url;
- (void)titlePage:(NSString *)title;

@end
