//
//  WebPageView.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 09.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebPageViewInput.h"
#import "WebPageViewOutput.h"

@interface WebPageView : UIViewController <WebPageViewInput>

@property (weak, nonatomic) id<WebPageViewOutput> output;

@end
