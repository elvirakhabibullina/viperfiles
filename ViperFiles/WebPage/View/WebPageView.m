//
//  WebPageView.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 09.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import "WebPageView.h"

@interface WebPageView () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *navbarView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navBarHeight;

@end

@implementation WebPageView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self configure];
    [_loadingIndicator startAnimating];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configure {
    [self.output requestPageInfo];
}

-(void)openURL:(NSURL *)url {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
//    [request setValue:[LanguagesManager manager].currentLanguageIdentifier forHTTPHeaderField:LANGUAGE_HEADER_IDENTIFEIR];
    [self.webView loadRequest:request];
    self.webView.delegate = self;
}

- (void)titlePage:(NSString *)title
{
    self.labelTitle.text = title;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // Alert error
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_loadingIndicator stopAnimating];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

-(IBAction)dismissMe:(id)sender {
    [self.output close];
}

@end
