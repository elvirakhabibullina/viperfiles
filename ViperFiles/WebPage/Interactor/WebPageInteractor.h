//
//  WebPageInteractor.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 09.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebPageInteractorInput.h"
#import "WebPageInteractorOutput.h"

@interface WebPageInteractor : NSObject <WebPageInteractorInput>

@property (weak, nonatomic) id<WebPageInteractorOutput> output;

@end
