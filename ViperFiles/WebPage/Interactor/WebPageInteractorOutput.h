//
//  WebPageInteractorOutpu.h
//  ViperFiles
//
//  Created by Elvira Khab on 19.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WebPageInteractorOutput <NSObject>

- (void)openURL:(NSURL *)url;
- (void)titlePage:(NSString *)title;

@end
