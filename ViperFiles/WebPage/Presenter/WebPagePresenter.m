//
//  WebPagePresenter.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 09.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import "WebPagePresenter.h"

@implementation WebPagePresenter

#pragma mark WebPageInteractorOutput

-(void)openURL:(NSURL *)url {
    [self.view openURL:url];
}

- (void)titlePage:(NSString *)title
{
    [self.view titlePage:title];
}

#pragma mark WebPageViewOutput

-(void)close {
    
}

-(void)requestPageInfo {
    
}
@end
