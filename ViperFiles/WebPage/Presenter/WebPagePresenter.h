//
//  WebPagePresenter.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 09.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebPageInteractorOutput.h"
#import "WebPageInteractorInput.h"
#import "WebPageViewInput.h"
#import "WebPageViewOutput.h"
#import "WebPageRouteOutput.h"
#import "WebPageRouteInput.h"
#import "WebPageModuleOutput.h"

@interface WebPagePresenter : NSObject <WebPageInteractorOutput, WebPageViewOutput, WebPageRouteOutput>

@property (strong, nonatomic) id<WebPageInteractorInput> interactor;
@property (nonatomic, strong) id<WebPageRouteInput> router;
@property (weak, nonatomic) id<WebPageViewInput> view;
@property (nonatomic, strong) id<WebPageModuleOutput> moduleOutput;

@end
