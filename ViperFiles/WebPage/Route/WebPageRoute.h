//
//  WebPageRoute.h
//  ViperFiles
//
//  Created by Elvira Khab on 19.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebPageRouteInput.h"
#import "WebPageRouteOutput.h"

@interface WebPageRoute : NSObject <WebPageRouteInput>

@property (nonatomic, weak) id<WebPageRouteOutput> output;

@end
