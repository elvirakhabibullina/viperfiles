//
//  TablePresenter.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 26.02.18.
//  Copyright © 2018 Дмитрий Гутовец. All rights reserved.
//

#import "TablePresenter.h"


@interface TablePresenter()

@property (nonatomic, strong) NSString *apartmentId;

@end

@implementation TablePresenter

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}



#pragma mark TableViewOutput

- (void)viewDidLoad {
    
}

- (void)reloadData {
    
}

#pragma mark TableInteractorOutput
- (void)reloadTableView {
    
}

@end
