//
//  TablePresenter.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 26.02.18.
//  Copyright © 2018 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TableInteractorInput.h"
#import "TableInteractorOutput.h"
#import "TableViewInput.h"
#import "TableViewOutput.h"
#import "TableRouterInput.h"
#import "TableRouterOutPut.h"
#import "TableModuleOutput.h"

@interface TablePresenter : NSObject <TableInteractorOutput, TableViewOutput, TableRouterOutput>

@property (nonatomic, weak) id<TableViewInput> view;
@property (nonatomic, strong) id<TableRouterInput> router;
@property (nonatomic, strong) id<TableInteractorInput> interactor;
@property (nonatomic, strong) id<TableModuleOutput> moduleOutput;

@end
