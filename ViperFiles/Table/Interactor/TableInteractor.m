//
//  TableInteractor.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 17.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import "TableInteractor.h"

@interface TableInteractor()


@end

@implementation TableInteractor

- (instancetype)init {
    if (self = [super init]) {
        
    }
    
    return self;
}

#pragma mark Private



#pragma mark TableInteractorInput - Table
- (void)loadTableData {

}

- (void)prepairTableData {
    
}

- (NSInteger)numberSections {
    return 0;
}

- (NSString *)titleSectionOfIndex:(NSInteger)section {
    return @"";
}

- (NSInteger)numberRowsInSections:(NSInteger)section {
    return 0;
}

- (NSDictionary *)dataItemForIndexPath:(NSIndexPath *)indexPath {
    
    return @{};
}


@end
