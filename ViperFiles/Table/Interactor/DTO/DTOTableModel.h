//
//  DTOTableModel.h
//  ViperFiles
//
//  Created by Elvira Khab on 20.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTOTableModel : NSObject

@property (nonatomic, assign) NSArray *items;

@end
