//
//  TableInteractor.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 17.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TableInteractorInput.h"
#import "TableInteractorOutput.h"
#import "TableDataDecoratorInput.h"

@interface TableInteractor : NSObject <TableInteractorInput>

@property (weak, nonatomic) id<TableInteractorOutput> output;
@property (strong, nonatomic) id<TableDataDecoratorInput> builder;

@end
