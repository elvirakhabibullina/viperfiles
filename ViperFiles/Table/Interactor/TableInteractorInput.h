//
//  TableInteractorProtocol.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 17.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

@protocol TableInteractorInput <NSObject>

- (void)prepairTableData;
- (void)loadTableData;

- (NSInteger)numberSections;
- (NSString *)titleSectionOfIndex:(NSInteger)section;
- (NSInteger)numberRowsInSections:(NSInteger)section;
- (NSDictionary *)dataItemForIndexPath:(NSIndexPath *)indexPath;


@end



