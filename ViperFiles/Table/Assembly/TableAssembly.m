//
//  TableAssembly.m
//  ViperFiles
//
//  Created by Elvira Khab on 20.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import "TableAssembly.h"
#import "TableRoute.h"
#import "TableRouterInput.h"
#import "TableRouterOutput.h"
#import "TableViewInput.h"
#import "TableViewOutput.h"
#import "TableView.h"
#import "TableInteractor.h"
#import "TableViewInput.h"
#import "TableViewOutput.h"
#import "TablePresenter.h"
#import "TableDataDecorator.h"


@implementation TableAssembly

+ (TableAssembly *)assembleWithModuleOutput:(id<TableModuleOutput>)moduleOutput {
    id<TableRouterInput> router = [[TableRoute alloc] init];
    id<TableInteractorInput> interactor = [[TableInteractor alloc] init];
    id<TableInteractorOutput, TableViewOutput, TableRouterOutput> presenter = [[TablePresenter alloc] init];
    
    id<TableViewInput> view = [[TableView alloc] initWithNibName:@"TableView" bundle:nil];
    id<TableDataDecoratorInput> builder = [[TableDataDecorator alloc] init];
//    TableManager *manager = [TableManager new];
    
    ((TableView *)view).output = presenter;
    ((TableInteractor *)interactor).output = presenter;
//    ((TableInteractor *)interactor).manager = manager;
    ((TableInteractor *)interactor).builder = builder;
    ((TablePresenter *)presenter).router = router;
    ((TablePresenter *)presenter).interactor = interactor;
    ((TablePresenter *)presenter).view = view;
    ((TablePresenter *)presenter).moduleOutput = moduleOutput;
    
    ((TableRoute *)router).output = presenter;
    
    TableAssembly *module = [[TableAssembly alloc] init];
    module.view = view;
    module.router = router;
    return module;
}

@end
