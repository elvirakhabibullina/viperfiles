//
//  TableAssembly.h
//  ViperFiles
//
//  Created by Elvira Khab on 20.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TableViewInput.h"
#import "TableRouterInput.h"
#import "TableModuleOutput.h"

@interface TableAssembly : NSObject

@property (nonatomic, strong, nonnull) id<TableViewInput> view;
@property (nonatomic, weak, nullable) id<TableRouterInput> router;

+ (TableAssembly *)assembleWithModuleOutput:(id<TableModuleOutput>)moduleOutput;

@end
