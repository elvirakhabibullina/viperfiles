//
//  TableModuleOutput.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 26.02.18.
//  Copyright © 2018 Дмитрий Гутовец. All rights reserved.
//

@class TableView;

@protocol TableModuleOutput <NSObject>

- (void)tableView:(TableView *)sender didFinishWithData:(NSDictionary *)data;

@end
