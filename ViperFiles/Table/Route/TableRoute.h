//
//  TableRoute.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 26.02.18.
//  Copyright © 2018 Дмитрий Гутовец. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TableRouterInput.h"
#import "TableRouterOutput.h"
#import "TableModuleOutput.h"

@interface TableRoute : NSObject <TableRouterInput>

@property (nonatomic, weak) id<TableRouterOutput> output;

@end
