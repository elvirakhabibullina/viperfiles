//
//  TableViewOutput.h
//  ViperFiles
//
//  Created by Elvira Khab on 19.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

@protocol TableViewOutput <NSObject>

- (void)viewDidLoad;
- (void)reloadData;

@end
