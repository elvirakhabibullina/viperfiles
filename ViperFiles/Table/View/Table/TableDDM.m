//
//  TableDDM.m
//  ViperFiles
//
//  Created by Elvira Khab on 19.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import "TableDDM.h"
#import "TableCellsFactory.h"

@interface TableDDM ()

@property (nonatomic, weak) id<TableViewOutput> viewOutput;
@property (nonatomic, strong) TableCellsFactory *factory;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation TableDDM

- (instancetype)initWithTable:(UITableView *)tableView andViewOutput:(id<TableViewOutput>)output {
    if (self = [super init]) {
        _viewOutput = output;
        _factory = [[TableCellsFactory alloc] init];
        
        _tableView = tableView;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
    }
    return self;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView new];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.factory tableView:tableView cellFor:1];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //output acitions
}

@end
