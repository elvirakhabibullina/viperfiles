//
//  TableCellsFactory.h
//  ViperFiles
//
//  Created by Elvira Khab on 20.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTOTableModel.h"

@interface TableCellsFactory : NSObject

- (UITableViewCell *)tableView:(UITableView *)tableView cellFor:(NSInteger)type;

@end
