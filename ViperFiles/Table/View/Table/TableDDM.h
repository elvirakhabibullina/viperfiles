//
//  TableDDM.h
//  ViperFiles
//
//  Created by Elvira Khab on 19.04.2018.
//  Copyright © 2018 Evlira Hab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewOutput.h"
#import "DTOTableModel.h"

@interface TableDDM : NSObject <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) DTOTableModel *model;

- (instancetype)initWithTable:(UITableView *)tableView andViewOutput:(id<TableViewOutput>) output;

@end
