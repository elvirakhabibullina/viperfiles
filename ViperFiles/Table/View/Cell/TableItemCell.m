//
//  TableItemCell.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 15.03.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import "TableItemCell.h"

@interface TableItemCell()

@end

@implementation TableItemCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"TableItemCell" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization coda
    
}

#pragma mark Public
- (void)configure:(NSDictionary *)data
{
    
}


@end
