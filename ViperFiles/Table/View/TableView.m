//
//  TableView.m
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 17.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import "TableView.h"
#import "TableDDM.h"

@interface TableView () 

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;
@property (weak, nonatomic) IBOutlet UIView *viewActivity;
@property (strong, nonatomic) TableDDM *tableDDM;

@end

@implementation TableView

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configDDM];
    [self configureActivity];
    [self.output viewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configDDM {
    self.tableDDM = [[TableDDM alloc] initWithTable:self.tableView andViewOutput:self.output];
}

- (void)configureActivity {
    self.viewActivity.backgroundColor = [UIColor colorWithWhite:1. alpha:0.8];
    self.viewActivity.layer.cornerRadius = 10.f;
    self.viewActivity.layer.masksToBounds = YES;
    
    self.activityView.tintColor = [UIColor grayColor];
    
    [self hideActivity];
}

-(void)refreshTable:(UIRefreshControl *)sender {
    [self.output reloadData];
}

#pragma mark - Table view data source

-(void)setLoaderVisible:(BOOL)visible {
    if (visible) {
        [self showActivity];
    } else {
        [self hideActivity];
    }
}

#pragma mark TableViewInterface

- (void)showActivity
{
    self.viewActivity.hidden = NO;
    [self.activityView startAnimating];
}

- (void)hideActivity
{
    self.viewActivity.hidden = YES;
    [self.activityView stopAnimating];
}

@end
