//
//  TableViewInput.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 17.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//


@protocol TableViewInput <NSObject>

- (void)setLoaderVisible:(BOOL)visible;

@end
