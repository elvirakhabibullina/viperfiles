//
//  TableView.h
//  aizk_iOS
//
//  Created by Дмитрий Гутовец on 17.02.17.
//  Copyright © 2017 Дмитрий Гутовец. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewInput.h"
#import "TableViewOutput.h"

@interface TableView : UIViewController <TableViewInput>

@property (weak, nonatomic) id<TableViewOutput> output;

@end
